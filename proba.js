"use strict";

// obtenir un nombre random dans un intervalle de 0 à max
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}


function testFrequence(max, sample) {
    //créer un objet avec comme clé le nombre et en valeur sa fréquence qui est d'abord de 0
    let frequences = [];
    let frequence_theorique = sample/max;
    for (let nombre = 0 ; nombre < max ; nombre ++) {
        frequences[nombre] = 0;
    }

    //choisis un nombre aléatoire sur un échantillon donné

    for (let turn = 1 ; turn <= sample; turn ++){
        let random = getRandomInt(max);

        // augmente sa fréquence à la clé correspondante
        frequences[random] += 1;
    }
    let frequence_max = Math.max(...frequences); //opérateur d'étalement car c'est un tableau et il attend une suite de chiffres
    let frequence_min = Math.min(...frequences);

    console.log(`Intervalle : de 0 à ${max}\n
Echantillon : ${sample}\n
La fréquence théorique est de ${frequence_theorique.toFixed(2)}\n
La fréquence minimale est ${frequence_min}\n
La fréquence maximale est ${frequence_max}`)
}

testFrequence(6, 20);

